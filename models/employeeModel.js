const mongoose = require("mongoose");

const employeeSchema = new mongoose.Schema({
  lastName: { type: String, required: true },
  surname: { type: String, required: true },
  name: { type: String, required: true },
  middleName: { type: String, required: true },
  country: { type: String, required: true },
  typeOfID: { type: String, required: true },
  idNumber: { type: String, required: true },
  email: { type: String, required: true },
  created_at: { type: Date },
  status: { type: String },
});

let Employee = mongoose.model("employee", employeeSchema);

module.exports = Employee;
