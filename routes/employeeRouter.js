const express = require("express");
const Employee = require("../models/employeeModel");

const router = express.Router();

router.post("/register", async (req, res) => {
  try {
    let {
      lastName,
      surname,
      name,
      middleName,
      country,
      typeOfID,
      idNumber,
      email,
    } = req.body;
    if (
      !lastName ||
      !surname ||
      !name ||
      !middleName ||
      !country ||
      !typeOfID ||
      !idNumber ||
      !email
    ) {
      return res.status(400).json({ msg: "Not all fields have been entered." });
    }

    if (lastName.length > 20 || surname.length > 20 || name.length > 20) {
      return res
        .status(400)
        .json({ msg: "Cannot be longer than 20 characters" });
    }

    if (middleName.length > 40) {
      return res
        .status(400)
        .json({ msg: "Cannot be longer than 40 characters" });
    }

    if (idNumber.length > 20) {
      return res
        .status(400)
        .json({ msg: "Id number cannot be longer than 20 characters" });
    }

    const existingEmployee = await Employee.findOne({ idNumber: idNumber });

    if (existingEmployee) {
      return res
        .status(400)
        .json({ msg: "An employee with this ID already exists." });
    }

    let date = new Date().toISOString().slice(0, 10);
    let created_at = date;
    let status = "ACTIVE";

    const newEmployee = new Employee({
      lastName,
      surname,
      name,
      middleName,
      country,
      typeOfID,
      idNumber,
      email,
      created_at,
      status,
    });
    const savedEmployee = await newEmployee.save();
    res.json(savedEmployee);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.get("/find", (req, res) => {
  Employee.find({}, function (err, employees) {
    if (err) console.warn(err);
    return res.json(employees);
  });
});

router.put("/find/:id", (req, res) => {
  let id = req.params.id;
  Employee.findOne({ _id: id }, function (err, employee) {
    if (err) {
      console.log(err);
      res.status(500).send();
    } else {
      if (!employee) {
        res.status(404).send();
      } else {
        if (req.body.lastName) {
          employee.lastName = req.body.lastName;
        }
        if (req.body.surname) {
          employee.surname = req.body.surname;
        }
        if (req.body.name) {
          employee.name = req.body.name;
        }
        if (req.body.middleName) {
          employee.middleName = req.body.middleName;
        }
        if (req.body.country) {
          employee.country = req.body.country;
        }
        if (req.body.typeOfID) {
          employee.typeOfID = req.body.typeOfID;
        }
        if (req.body.idNumber) {
          employee.idNumber = req.body.idNumber;
        }
        if (req.body.email) {
          employee.email = req.body.email;
        }
        employee.save(function (err, updateEmployee) {
          if (err) {
            console.log(err);
            res.status(500).send();
          } else {
            res.send(updateEmployee);
          }
        });
      }
    }
  });
});

module.exports = router;
